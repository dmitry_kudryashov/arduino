
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
RF24 radio(9, 10); // Определяем рабочие ножки;
const uint64_t pipe = 0xE8E8F0F0E1LL; // Определяем адрес рабочей трубы;

void setup() {
	Serial.begin(115200);
  radio.begin(); // Старт работы;
  radio.enableAckPayload();  // Разрешение отправки нетипового ответа передатчику;
  radio.openReadingPipe(1, pipe); // Открываем трубу и
  radio.startListening();  //начинаем слушать;
}
/* В лупе */
void loop() {
  uint32_t message = 111;  //Вот какой потенциальной длины сообщение - uint32_t!
  //туда можно затолкать значение температуры от датчика или еще что-то полезное.
  radio.writeAckPayload( 1, &message, sizeof(message) ); // Грузим сообщение для автоотправки;
  byte pipeNo, gotByte;                          // Declare variables for the pipe and the byte received
    while( radio.available(&pipeNo)){              // Read all available payloads
      radio.read( &gotByte, 1 );                   
                                                   // Since this is a call-response. Respond directly with an ack payload.
      gotByte += 1;                                // Ack payloads are much more efficient than switching to transmit mode to respond to a call
      //radio.writeAckPayload(pipeNo,&gotByte, 1 );  // This can be commented out to send empty payloads.
      Serial.println("Pipe nomer ");
      Serial.println(pipeNo);  
      Serial.print(F("Loaded next response "));
      Serial.println(gotByte);  
   }
}
