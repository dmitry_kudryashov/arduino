#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

//enums
/*
 	DeviceMode is used to change currnt device mode
*/
enum DeviceMode {
	DeviceModeReceiver,
	DeviceModeTransmitter,
};

//defines
#define DEBUG 1

//constants
const int PIN_CE = 9;
const int PIN_CSN = 10;
const uint64_t PIPE = 0xE8E8F0F0E1LL;
const enum DeviceMode currentDeviceMode = DeviceModeReceiver;



//global variables

RF24 radio(PIN_CE, PIN_CSN);

//first initialization
void setup() {
	// put your setup code here, to run once:

#ifdef DEBUG
	Serial.begin(115200);
#endif

	//create PIPE to receive or to transmit
	radio.begin();
	radio.enableAckPayload();

	switch (currentDeviceMode) {
		case DeviceModeReceiver:
			radio.openReadingPipe(1, PIPE);
			radio.startListening();
			break;
		case DeviceModeTransmitter:
			radio.openWritingPipe(PIPE);
			break;
	}
}

void loop() {
	uint32_t message;

	//code for transmitter
	if (currentDeviceMode == DeviceModeTransmitter) {
		static int command = 0;
		radio.write( &command, sizeof(command) );  //send command;
		if ( radio.isAckPayloadAvailable() ) {  // Waiting to recieve...
			radio.read(&message, sizeof(message)); //Read a message from receiver
		}
		command++;
#ifdef DEBUG
		Serial.print(F("Confirmation message is  "));
		Serial.println(message);
#endif
		delay(500);
	}
	else if (currentDeviceMode == DeviceModeReceiver) {
		uint32_t message = 111;
		radio.writeAckPayload(1, &message, sizeof(message));
		byte pipeNo;
		int command = 0;
		while ( radio.available(&pipeNo)) {            // Read all available payloads
			radio.read(&command, sizeof(command));
			//radio.writeAckPayload(pipeNo,&command, sizeof(command));  // This can be commented out to send empty payloads.
#ifdef DEBUG
			Serial.print(F("Loaded next response "));
			Serial.println(command);
#endif
		}
	}





}
