/* В дефайнах */
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
RF24 radio(9, 10);
const uint64_t pipe = 0xE8E8F0F0E1LL;
uint32_t message;  // Эта переменная для сбора обратного сообщения от приемника;
/* В сетапе */
void setup() {
	radio.begin();
	radio.enableAckPayload();
	radio.openWritingPipe(pipe);
}

int command = 0;  // Не суть - приемнику надо что-то передать, но это может быть и полезная информация;
void loop() {

	radio.write( &command, sizeof(command) );  //Отправляем команду;
	if ( radio.isAckPayloadAvailable() ) {  // Ждем получения...
		radio.read(&message, sizeof(message)); //... и имеем переменную message с числом 111 от приемника.
	}
	command++;
	delay(500);
}
