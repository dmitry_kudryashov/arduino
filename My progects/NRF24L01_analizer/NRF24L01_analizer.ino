#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include <Wire.h>

#define CE_PIN	7
#define CSN_PIN 8
 
RF24 radio(CE_PIN,CSN_PIN);
// Set up nRF24L01 radio on SPI bus plus CE and CSN
// CE	- Chip Enable
// CSN	- Chip Select

#define OLED_RESET 4
#define DISPLAY_ID 0x3C
Adafruit_SSD1306 display(OLED_RESET);

 #if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

const uint8_t num_channels = 125;
const int num_reps = 40;
const int delayMicroSeconds = 100;

uint8_t values[num_channels];
int drawnChanel = 0;

void setup() {
	radio.begin();
	radio.setAutoAck(false);
 //	radio.setDataRate(RF24_1MBPS);
	radio.startListening();
	radio.stopListening();
 
	display.begin(SSD1306_SWITCHCAPVCC, DISPLAY_ID);	// initialize with the I2C addr 0x3D (for the 128x64)
	display.clearDisplay();

	const int yMax = display.height() - num_reps - 2;
	
	display.drawLine(0, yMax - 1, display.width() - 1, yMax - 1, WHITE);
	display.drawLine(0, yMax, display.width() - 1, yMax, BLACK);
	display.display();
	memset(values,0,sizeof(values));
}

void loop() {
	for (int chanel = 0; chanel < num_channels; chanel++) {
		values[chanel] = 0;
		radio.setChannel(chanel);
		for (int attempt = 0; attempt < num_reps; attempt++) {
			radio.startListening();
			delayMicroseconds(delayMicroSeconds);
			if ( radio.testCarrier() ){
				++values[chanel];
			}
			radio.stopListening();
		}
		int value = values[chanel];
		display.drawLine(chanel, display.height()-1, chanel, display.height()-1 - value, WHITE);
		int nextChanel = chanel + 1;
		nextChanel = nextChanel < num_channels ? nextChanel : 0;
		
		value = values[nextChanel];
		display.drawLine(nextChanel, display.height()-1, nextChanel, display.height()-1 - value, BLACK);
		
//		display.display();
		wtriteChanelNumber(chanel);
		}
}

void wtriteChanelNumber(int chanel) {
	display.setTextSize(1);
	display.setTextColor(BLACK);
	display.setCursor(0,0);
	display.print("chanel: ");
	display.print(drawnChanel);
	display.setCursor(0,10);
	display.print("frequency: ");
	display.print(drawnChanel + 2400);
	display.print("MHz");

	drawnChanel = chanel;

	display.setTextColor(WHITE);
	display.setCursor(0,0);
	display.print("chanel: ");
	display.print(drawnChanel);
	display.setCursor(0,10);
	display.print("frequency: ");
	display.print(drawnChanel + 2400);
	display.print("MHz");
		
	display.display();
}
