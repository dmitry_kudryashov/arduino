/*********************************************************************
This is an example sketch for our Monochrome Nokia 5110 LCD Displays

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products/338

These displays use SPI to communicate, 4 or 5 pins are required to
interface

Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!

Written by Limor Fried/Ladyada  for Adafruit Industries.
BSD license, check license.txt for more information
All text above, and the splash screen must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

// Software SPI (slower updates, more flexible pin options):
// pin 3 - Serial clock out (SCLK)
// pin 4 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 7 - LCD chip select (CS)
// pin 6 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 7, 6);

// Hardware SPI (faster, but must use certain hardware pins):
// SCK is LCD serial clock (SCLK) - this is pin 13 on Arduino Uno
// MOSI is LCD DIN - this is pin 11 on an Arduino Uno
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
// Adafruit_PCD8544 display = Adafruit_PCD8544(5, 4, 3);
// Note with hardware SPI MISO and SS pins aren't used but will still be read
// and written to during SPI transfer.  Be careful sharing these pins!

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2
#define DEFAULT_DELLAY 500

#define LOGO16_GLCD_HEIGHT 16
#define LOGO16_GLCD_WIDTH  16

void setup()   {
  	Serial.begin(9600);
    display.begin();
    // you can change the contrast around to adapt the display
	// for the best viewing!
	display.setContrast(60);
}


void loop() {


 // display.display(); // show splashscreen
  // invert the display

  display.clearDisplay();   // clears the screen and buffer
 // delay(DEFAULT_DELLAY);
  // draw many lines
  display.drawLine(0, 0, display.width()-1, display.height()-1, BLACK);
  display.display();
  delay(DEFAULT_DELLAY);
  display.clearDisplay();

  display.drawRect(0, 0, display.width() / 2, display.height() / 2, BLACK);
    display.display();
    delay(DEFAULT_DELLAY);
  display.clearDisplay();
	 // draw a single pixel
  display.drawPixel(10, 10, BLACK);
  display.display();
  delay(DEFAULT_DELLAY);

//
//randomSeed(666);     // whatever seed
//random(display.width());

	display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);

  for (uint8_t i=0; i < 168; i++) {
    if (i == '\n') continue;
    display.write(i);
    //if ((i > 0) && (i % 14 == 0))
//      display.println();
      display.display();
//      delay(DEFAULT_DELLAY);
  }    
  display.display();

  // draw the first ~12 characters in the font
  
  display.display();
  delay(DEFAULT_DELLAY);
  display.clearDisplay();

  // text display tests
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println("Hello, world!");
  display.setTextColor(WHITE, BLACK); // 'inverted' text
  display.println(3.141592);
  display.setTextSize(2);
 // display.setTextColor(BLACK);
  display.print("0x"); display.println(0xDEADBEEF, HEX);
  display.display();
  delay(DEFAULT_DELLAY);

  // rotation example
  display.clearDisplay();
  display.setRotation(1);  // rotate 90 degrees counter clockwise, can also use values of 2 and 3 to go further.
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println("Rotation");
  display.setTextSize(2);
  display.println("Example!");
  display.display();
  delay(DEFAULT_DELLAY);

  // revert back to no rotation
  display.setRotation(0);

}


