#include "nRF24L01.h"
#include "RF24.h"

//#define RECIVER
#define SENDER

#ifdef RECIVER
const int motor1_x = 4;
const int motor1_y = 5;
const int motor1_shim = 6;

const int motor2_x = A0;
const int motor2_y = A1;
const int motor2_shim = 3;
#endif

#ifdef SENDER
const int analog_pin_x = A2;
const int analog_pin_y = A1;
const int buton_pin = 0;

const int shim_max = 1023;
const int shim_min = 0;
const int shim_delta = 40;
const int shim_midle = 512;
#endif

struct MotorData{
	bool out1 = false;
	bool out2 = false;
	int shim = 0;
};

struct SendData {
	MotorData motorA;
	MotorData motorB;
	bool isButtonClicked = false;
};

const int CE_PIN = 7; // CE	- Chip Enable
const int CSN_PIN = 8; // CSN	- Chip Select
const uint64_t PIPE = 0xE8E8F0F0E1LL;
const uint32_t MESSAGE_IS_OK = 111;

RF24 radio(CE_PIN,CSN_PIN);

void setup() {
	#ifdef SENDER
	pinMode(analog_pin_x, INPUT);
	pinMode(analog_pin_y, INPUT);
	
	radio.begin();
	radio.enableAckPayload();
	radio.openWritingPipe(PIPE);
	#endif

	#ifdef RECIVER
	pinMode(motor1_x, OUTPUT);
	pinMode(motor1_y, OUTPUT);
	pinMode(motor1_shim, OUTPUT);

	pinMode(motor2_x, OUTPUT);
	pinMode(motor2_y, OUTPUT);
	pinMode(motor2_shim, OUTPUT);

	radio.begin();
	radio.enableAckPayload();
	radio.openReadingPipe(1, PIPE); // Открываем трубу и
	radio.startListening();  //начинаем слушать;
	#endif
}

void loop() {
	#ifdef SENDER
	int jX = analogRead(analog_pin_x);
	int jY = analogRead(analog_pin_y);

	SendData sendedData;
	// print out the state of the button:

	int lext_shim = jY;
	int right_shim = jY;
	lext_shim += jX - shim_midle;
	right_shim -= jX - shim_midle;

	
	int x_shim = abs(lext_shim - shim_midle);
	x_shim = max(x_shim, 0);

	if (x_shim > shim_delta) {
		x_shim *= 2;
	} else {
		x_shim = 0;
	}
	
	sendedData.motorA.out1 = (lext_shim - shim_midle) > 0;
	sendedData.motorA.out2 = !sendedData.motorA.out1;
	sendedData.motorA.shim = min(shim_max, x_shim);

	
	int y_shim = abs(right_shim - shim_midle);
	y_shim = max(y_shim, 0);
	
	if (y_shim > shim_delta) {
		y_shim *= 2;
	} else {
		y_shim = 0;
	}
	
	sendedData.motorB.out1 = (right_shim - shim_midle) > 0;;
	sendedData.motorB.out2 = !sendedData.motorB.out1;
	sendedData.motorB.shim = min(shim_max, y_shim);
	
	sendData(sendedData);
	delay(10);
	#endif
	
	#ifdef RECIVER
	SendData data = reciveData();
  	

	digitalWrite(motor1_x, data.motorA.out1);
	digitalWrite(motor1_y, data.motorA.out2);
	analogWrite(motor1_shim, data.motorA.shim);

	digitalWrite(motor2_x, data.motorB.out1);
	digitalWrite(motor2_y, data.motorB.out2);
	analogWrite(motor2_shim, data.motorB.shim);

   	delay(50);
	#endif
}

SendData reciveData() {
	uint32_t message;  // Эта переменная для сбора обратного сообщения от приемника;
	SendData returnValue;
	radio.writeAckPayload( 1, &message, sizeof(message) ); // Грузим сообщение для автоотправки;
  	byte pipeNo, gotByte;
	if(radio.available()){
		radio.read(&returnValue, sizeof(returnValue));
	}
//	radio.writeAckPayload();
//    while( radio.available(&pipeNo)){              // Read all available payloads
//      radio.read( &gotByte, 1 );                   
//                                                   // Since this is a call-response. Respond directly with an ack payload.
//      gotByte += 1;                                // Ack payloads are much more efficient than switching to transmit mode to respond to a call
//      //radio.writeAckPayload(pipeNo,&gotByte, 1 );  // This can be commented out to send empty payloads.
//      Serial.println("Pipe nomer ");
//      Serial.println(pipeNo);  
//      Serial.print(F("Loaded next response "));
//      Serial.println(gotByte);  
//	}
	return returnValue;
}

void sendData(SendData const &data) {
	uint32_t message = 0;
	bool returnValue = false;
	radio.write( &data, sizeof(data) );
//	if ( radio.isAckPayloadAvailable() ) {
//		radio.read(&message, sizeof(message));
//		returnValue = message == MESSAGE_IS_OK;
//	}
//	return returnValue;
}


