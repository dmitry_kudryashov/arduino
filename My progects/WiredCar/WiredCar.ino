

int analog_pin_x = A1;
int analog_pin_y = A0;
int buton_pin = 0;

int motor1_x = 10;
int motor1_y = 11;
int motor1_shim = 6;

int motor2_x = 9;
int motor2_y = 8;
int motor2_shim = 5;

int shim_max = 1023;
int shim_min = 0;
int shim_delta = 40;
int shim_midle = 512;

void setup() {
  // put your setup code here, to run once:
	Serial.begin(9600);
	pinMode(analog_pin_x, INPUT);
	pinMode(analog_pin_y, INPUT);

	pinMode(motor1_x, OUTPUT);
	pinMode(motor1_y, OUTPUT);
	pinMode(motor1_shim, OUTPUT);

	pinMode(motor2_x, OUTPUT);
	pinMode(motor2_y, OUTPUT);
	pinMode(motor2_shim, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
	int jX = analogRead(analog_pin_x);
	int jY = analogRead(analog_pin_y);
	// print out the state of the button:

	int lext_shim = jY;
	int right_shim = jY;
	lext_shim += jX - shim_midle;
	right_shim -= jX - shim_midle;

	int x_1 = (lext_shim - shim_midle) > 0;
	int x_2 = !x_1;
	int x_shim = abs(lext_shim - shim_midle);
	x_shim = max(x_shim, 0);

	if (x_shim > shim_delta) {
		x_shim *= 2;
	} else {
		x_shim = 0;
	}
	x_shim = min(shim_max, x_shim);

	int y_1 = (right_shim - shim_midle) > 0;;
	int y_2 = !y_1;
	int y_shim = abs(right_shim - shim_midle);
	y_shim = max(y_shim, 0);
	
	if (y_shim > shim_delta) {
		y_shim *= 2;
	} else {
		y_shim = 0;
	}
	y_shim = min(shim_max, y_shim);

	digitalWrite(motor1_x, x_1);
	digitalWrite(motor1_y, x_2);
	analogWrite(motor1_shim, x_shim);
//	
	digitalWrite(motor2_x, y_1);
	digitalWrite(motor2_y, y_2);
	analogWrite(motor2_shim, y_shim);


	
	delay(1);        // delay in between reads for stability

}
